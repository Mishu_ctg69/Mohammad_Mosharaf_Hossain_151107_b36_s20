<?php
namespace App;

class Person
{
    public $name = "Mishu";
    public $gender = "Male";
    public $blood_group = "B+";

    public function showPersoninfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}