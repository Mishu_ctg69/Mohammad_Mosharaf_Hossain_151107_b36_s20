<?php

    use App\Person;
    use App\Student;

    function __autoload($className){
        $arr = explode("\\",$className);
        require_once("../../src/BITM/SEIP151107/".$arr[1].".php");

    }


    $objPerson = new Person();
    $objPerson->showPersoninfo();

    $objPerson = new Student();
    $objPerson->showStudentInfo();

